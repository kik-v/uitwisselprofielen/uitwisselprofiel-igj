from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Zvw-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wmo-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Wlz-indicatie (Geen Sector))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Wlz-indicatie (4VV))",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel Zorgproces + Wmo-indicatie)",
        "Amount": 10, 
        "Human": [
            {                
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_1_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')
    test.set_reference_date_to("2023-01-01")
    test.set_branch_number_to("'000001254'")

    # Assertions
    test.verify_header_present("aantal_wlz")
    test.verify_header_present("aantal_zvw")
    test.verify_header_present("aantal_overig")
    test.verify_header_present("aantal_uniek")

def test_if_number_of_rows_returned_is_correct_for_query_1_1_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')
    test.set_reference_date_to("2023-01-01")
    test.set_branch_number_to("'000001254'")
    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_1_1_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

    test.set_reference_date_to("2024-01-01")
    test.set_branch_number_to("'000001254'")

    # Assertions
    test.verify_value("aantal_wlz","16")
    # test.verify_value("aantal_zvw","0")
    # test.verify_value("aantal_overig","0")
    # test.verify_value("aantal_uniek","10")

def test_if_dates_can_change_1_1_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

    test.set_reference_date_to("2023-01-01")
    test.set_branch_number_to("'000001254'")

    # Assertions
    test.verify_value("aantal_wlz","13")
    # test.verify_value("aantal_zvw","0")
    # test.verify_value("aantal_overig","0")
    # test.verify_value("aantal_uniek","10")

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_1_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001254'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("16","16.0"))#, where_conditions=[("vestiging","000001254")])
        # test.verify_value("aantal_zvw","0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_overig","0",where_condition=("vestiging","000001254"))
        # test.verify_value("aantal_uniek","10",where_condition=("vestiging","000001254"))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_1_1_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("18","18.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_1_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("18","18.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_1_1_02_a(db_config):
    """ Testcase 02a (Wel Zorgproces + Wel Wlz-Indicatie (Niet op peildatum))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("18","18.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_1_1_1_02_b(db_config):
    """ Testcase 02b (Wel Zorgproces + Wel Zvw-Indicatie (Niet op peildatum))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query    
        # test.verify_value("aantal_wlz","0")
        test.verify_value_in_list("aantal_zvw",("22","22.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_overig","0")
        # test.verify_value_in_list("aantal_uniek",("10","10.0"))#, where_conditions=[("vestiging","000001287")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_1_1_1_02_c(db_config):
    """ Testcase 02c (Wel Zorgproces + Wel Wmo-Indicatie (Niet op peildatum))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value("aantal_wlz","0")
        # test.verify_value("aantal_zvw","0")
        test.verify_value_in_list("aantal_overig",("20","20.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_1_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("18","18.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_1_1_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("18","18.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_1_04(db_config):
    """ Testcase 04 (Geen Zorgproces + Wel Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("28","28.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_1_1_1_04_a(db_config):
    """ Testcase 04a (Wel Zorgproces + Wel Wlz-Indicatie (4VV))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_wlz",("28","28.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_zvw","0")
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_1_1_1_04_b(db_config):
    """ Testcase 04b (Wel Zorgproces + Wel Zvw-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value("aantal_wlz","0")
        test.verify_value_in_list("aantal_zvw",("22","22.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_overig","0")
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_1_1_1_04_c(db_config):
    """ Testcase 04c (Wel Zorgproces + Wel Wlz-Indicatie (2LVG))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.1 Aantal cliënten per wet
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value("aantal_wlz","0")
        # test.verify_value("aantal_zvw","0")
        test.verify_value_in_list("aantal_overig",("20","20.0"))#, where_conditions=[("vestiging","000001287")])
        # test.verify_value("aantal_uniek","10")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()