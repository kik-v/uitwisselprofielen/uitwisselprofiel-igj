from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

td_template = [
    {
        "Description": "Testcase template (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2023-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. + Geen DAN Dienst (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
  
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
  
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie + Geen AOK + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INHOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen ZVL functie + Wel AOK (OBPT) + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
  
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    { 
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Wel AOK (OBPT) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09 = [
    {
        "Description": "Testcase 09 (Geen ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    { 
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_10 = [
    {
        "Description": "Testcase 10 (Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    { 
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_11 = [
    {
        "Description": "Testcase 11 (Wel ZVL functie + Wel AOK OBPT + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_12 = [
    {
        "Description": "Testcase 12 (Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_13 = [
    {
        "Description": "Testcase 13 (Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_13_a = [
    {
        "Description": "Testcase 13a (Wel ZVL functie + Geen AOK (UITZ OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Utizend OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_13_b = [
    {
        "Description": "Testcase 13b (Wel ZVL functie + Geen AOK (ST OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_13_c = [
    {
        "Description": "Testcase 13c (Wel ZVL functie + Geen AOK (VW OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VW OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_14 = [
    {
        "Description": "Testcase 14 (Wel ZVL functie + Wel AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_15 = [
    {
        "Description": "Testcase 15 (Wel ZVL functie + Wel AOK + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_16 = [
    {
        "Description": "Testcase 16 (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_16_a = [
    {
        "Description": "Testcase 16a (Wel ZVL functie + Wel AOK BPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_16_b = [
    {
        "Description": "Testcase 16b (Wel ZVL functie + Wel AOK BBL + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "AOK BBL Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_16_c = [
    {
        "Description": "Testcase 16c (Wel ZVL functie + Geen AOK (AOK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


td_16_d = [
    {
        "Description": "Testcase 16d (Wel ZVL functie + Wel AOK (Oproep OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "OproepOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Oproep OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17 = [
    {
        "Description": "Testcase 17 (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 2 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 2",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-01T23:59:59",
                                "location": "Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_a = [
    {
        "Description": "Testcase 17a (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 3 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_b = [
    {
        "Description": "Testcase 17b (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 4 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_c = [
    {
        "Description": "Testcase 17c (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 5 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_d = [
    {
        "Description": "Testcase 17d (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 6 + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_e = [
    {
        "Description": "Testcase 17e (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. Behandelaar + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_f = [
    {
        "Description": "Testcase 17f (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. Leerling + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_17_g = [
    {
        "Description": "Testcase 17g (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. Overig Zorgpersoneel + Wel DAN dienst (D+A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Overig zorgpersoneel",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T06:00:00",
                                "end_datetime": "2024-01-02T00:00:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18 = [
    {
        "Description": "Testcase 18 (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T07:00:00",
                                "end_datetime": "2024-01-01T15:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18_a = [
    {
        "Description": "Testcase 18a (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (A, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 6.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T16:00:00",
                                "end_datetime": "2024-01-01T22:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18_b = [
    {
        "Description": "Testcase 18b (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 7.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T23:00:00",
                                "end_datetime": "2024-01-02T06:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18_c = [
    {
        "Description": "Testcase 18c (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 13.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T07:00:00",
                                "end_datetime": "2024-01-01T22:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18_d = [
    {
        "Description": "Testcase 18d (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 15.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T23:00:00",
                                "end_datetime": "2024-01-02T15:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_18_e = [
    {
        "Description": "Testcase 18e (Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (A+N, Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 15.59,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T16:00:00",
                                "end_datetime": "2024-01-02T06:59:00",
                                "location": "Vestiging_Grotestraat"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_4_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Assertions
        test.verify_header_present('kwalificatieniveau')
        test.verify_header_present('Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst')
        test.verify_header_present('Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst')
        test.verify_header_present('Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst')
    finally:
        dg.delete_graph_data()

def test_if_number_of_rows_returned_is_correct_for_query_1_4_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Assertions
        test.verify_row_count(1)
    finally:
        dg.delete_graph_data()

def test_if_indicator_has_correct_value_for_query_1_4_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')


        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Assertions
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])
    finally:
        dg.delete_graph_data()

def test_if_dates_can_change_1_4_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        test.set_reference_date_new_param_to("2023-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Assertions
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))#("1","1.0"), where_conditions=[("kwalificatieniveau","Totaal")])
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
    finally:
        dg.delete_graph_data()

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_4_1_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. + Geen DAN Dienst (Clienten, Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_4_1_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_4_1_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_1_4_1_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Geen AOK + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_4_1_04(db_config):
    """ Testcase 04 (Geen ZVL functie + Wel AOK + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
        #     
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_1_4_1_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_1_4_1_06(db_config):
    """ Testcase 06 (Geen ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_1_4_1_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Wel AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_1_4_1_08(db_config):
    """ Testcase 08 (Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09
def test_if_value_returned_is_correct_for_query_1_4_1_09(db_config):
    """ Testcase 09 (Geen ZVL functie + Wel AOK + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 10
def test_if_value_returned_is_correct_for_query_1_4_1_10(db_config):
    """ Testcase 10 (Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_10)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 11
def test_if_value_returned_is_correct_for_query_1_4_1_11(db_config):
    """ Testcase 11 (Wel ZVL functie + Wel AOK + Geen Kwal. Niv. + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_11)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 12
def test_if_value_returned_is_correct_for_query_1_4_1_12(db_config):
    """ Testcase 12 (Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_12)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 13
def test_if_value_returned_is_correct_for_query_1_4_1_13(db_config):
    """ Testcase 13 (Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_13)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("0", "0"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 13a
def test_if_value_returned_is_correct_for_query_1_4_1_13_a(db_config):
    """ Testcase 13a (Wel ZVL functie + Geen AOK (UITZ OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_13_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("0","0"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 13b
def test_if_value_returned_is_correct_for_query_1_4_1_13_b(db_config):
    """ Testcase 13b (Wel ZVL functie + Geen AOK (ST OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_13_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 13c
def test_if_value_returned_is_correct_for_query_1_4_1_13_c(db_config):
    """ Testcase 13c (Wel ZVL functie + Geen AOK (VW OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_13_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 14
def test_if_value_returned_is_correct_for_query_1_4_1_14(db_config):
    """ Testcase 14 (Wel ZVL functie + Wel AOK (OBPT) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_14)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 15
def test_if_value_returned_is_correct_for_query_1_4_1_15(db_config):
    """ Testcase 15 (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Geen DAN dienst (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_15)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", (None,None))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", (None,None))    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", (None,None)) 
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16
def test_if_value_returned_is_correct_for_query_1_4_1_16(db_config):
    """ Testcase 16 (Wel ZVL functie + Wel AOK (OBPT) +  Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
           
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16a
def test_if_value_returned_is_correct_for_query_1_4_1_16_a(db_config):
    """ Testcase 16a (Wel ZVL functie + Wel AOK (BPT) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16b
def test_if_value_returned_is_correct_for_query_1_4_1_16_b(db_config):
    """ Testcase 16b (Wel ZVL functie + Wel AOK (BBL) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16c
def test_if_value_returned_is_correct_for_query_1_4_1_16_c(db_config):
    """ Testcase 16c (Wel ZVL functie + Wel AOK (AOK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 16d
def test_if_value_returned_is_correct_for_query_1_4_1_16_d(db_config):
    """ Testcase 16d (Wel ZVL functie + Wel AOK (Oproep OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_16_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
                    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17
def test_if_value_returned_is_correct_for_query_1_4_1_17(db_config):
    """ Testcase 17 (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 2 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_conditions=[("kwalificatieniveau","Kwalificatieniveau 2")])
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_conditions=[("kwalificatieniveau","Kwalificatieniveau 2")])    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 2")])  

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17a
def test_if_value_returned_is_correct_for_query_1_4_1_17_a(db_config):
    """ Testcase 17a (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 3 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (N-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 3"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 3"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 3")])  
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17b
def test_if_value_returned_is_correct_for_query_1_4_1_17_b(db_config):
    """ Testcase 17b (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 4 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Kwalificatieniveau 4"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10", where_condition=("kwalificatieniveau", "Kwalificatieniveau 4"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 4")])  
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17c
def test_if_value_returned_is_correct_for_query_1_4_1_17_c(db_config):
    """ Testcase 17c (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 5 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Kwalificatieniveau 5"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10", where_condition=("kwalificatieniveau", "Kwalificatieniveau 5"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 5")])  
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17d
def test_if_value_returned_is_correct_for_query_1_4_1_17_d(db_config):
    """ Testcase 17d (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 6 + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Kwalificatieniveau 6"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10", where_condition=("kwalificatieniveau", "Kwalificatieniveau 6"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 6")])  
            
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17e
def test_if_value_returned_is_correct_for_query_1_4_1_17_e(db_config):
    """ Testcase 17e (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. Behandelaar + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Behandelaar"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10", where_condition=("kwalificatieniveau", "Behandelaar"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Behandelaar")])             
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17f
def test_if_value_returned_is_correct_for_query_1_4_1_17_f(db_config):
    """ Testcase 17f (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. Leerling + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Leerling"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10",where_condition=("kwalificatieniveau","Leerling")) 
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Leerling")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 17g
def test_if_value_returned_is_correct_for_query_1_4_1_17_g(db_config):
    """ Testcase 17g (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. Overig zorgpersoneel + Wel DAN dienst (D+A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_17_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "10",where_condition=("kwalificatieniveau","Overig zorgpersoneel"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "10",where_condition=("kwalificatieniveau","Overig zorgpersoneel"))    
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Overig zorgpersoneel")])    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18
def test_if_value_returned_is_correct_for_query_1_4_1_18(db_config):
    """ Testcase 18 (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (D, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18a
def test_if_value_returned_is_correct_for_query_1_4_1_18_a(db_config):
    """ Testcase 18a (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (A, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18b
def test_if_value_returned_is_correct_for_query_1_4_1_18_b(db_config):
    """ Testcase 18b (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18c
def test_if_value_returned_is_correct_for_query_1_4_1_18_c(db_config):
    """ Testcase 18c (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (D+A, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("10", "10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18d
def test_if_value_returned_is_correct_for_query_1_4_1_18_d(db_config):
    """ Testcase 18d (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (D+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 18e
def test_if_value_returned_is_correct_for_query_1_4_1_18_e(db_config):
    """ Testcase 18e (Wel ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Wel DAN dienst (A+N, Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.4.1. Aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_18_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_dagdienst", "0",where_condition=("kwalificatieniveau","Kwalificatieniveau 1"))
        test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_avonddienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
        # test.verify_value_in_list("Aantal_PIL_met_een_zorgfunctie_in_de_nachtdienst", ("10","10.0"),where_conditions=[("kwalificatieniveau","Kwalificatieniveau 1")])    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()