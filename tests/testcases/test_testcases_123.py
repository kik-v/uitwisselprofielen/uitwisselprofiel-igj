from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:
td_template_1 = [
    {
        "Description": "Testcase template 2 (Employees with indeterminate contract + All qualification levels + 8 worked hours)",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date" : "2024-01-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 2",
                                        "start_date": "2024-02-01",
                                        "end_date" : "2024-02-28"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-03-01",
                                        "end_date" : "2024-03-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-04-01",
                                        "end_date" : "2024-04-30"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2024-05-01",
                                        "end_date" : "2024-05-30"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-06-01",
                                        "end_date" : "2024-06-30"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-07-01",
                                        "end_date" : "2024-07-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-08-01",
                                        "end_date" : "2024-08-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Overig zorgpersoneel",
                                        "start_date": "2024-09-01",
                                        "end_date" : "2024-09-30"
                                    }                                            
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            },
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-05-01T09:00:00",
                                "end_datetime": "2024-05-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ],
    }
]


td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. (Vest. 1254))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK + Geen Kwal. Niv. (Vest. 1287))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Geen AOK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie + Wel WOK + Geen Kwal. Niv. (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen ZVL functie + Wel AOK + Geen Kwal. Niv. (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 6 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Geen ZVL + AOK OBPT ",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            },
                            {
                                "label": "Geen ZVL + AOK OBPT ",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Geen ZVL functie + Wel AOK BPT + Kwal. Niv. Leerling (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Geen AOK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel ZVL functie + Wel AOK OBPT + Geen Kwal. Niv. (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel ZVL functie + AOK OBPT + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wel ZVL functie + AOK BPT + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wel ZVL functie + Oproep OK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "OproepOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Oproep OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Wel ZVL functie + AOK BBL + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "AOK BBL Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_d = [
    {
        "Description": "Testcase 08d (Wel ZVL functie + INH OK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_e = [
    {
        "Description": "Testcase 08e (Wel ZVL functie + UITZ OK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "UITZ OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_f = [
    {
        "Description": "Testcase 08f (Wel ZVL functie + ST OK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_g = [
    {
        "Description": "Testcase 08g (Wel ZVL functie + VWOK + Kwal. Niv. 1 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "VWOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09 = [
    {
        "Description": "Testcase 09 (Wel ZVL functie + AOK OBPT + Kwal. Niv. 2 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 2",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_a = [
    {
        "Description": "Testcase 09a (Wel ZVL functie + AOK OBPT + Kwal. Niv. 3 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_b = [
    {
        "Description": "Testcase 09b (Wel ZVL functie + AOK OBPT + Kwal. Niv. 4 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_c = [
    {
        "Description": "Testcase 09c (Wel ZVL functie + AOK OBPT + Kwal. Niv. 5 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_d = [
    {
        "Description": "Testcase 09d (Wel ZVL functie + AOK OBPT + Kwal. Niv. 6 (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_e = [
    {
        "Description": "Testcase 09e (Wel ZVL functie + AOK OBPT + Kwal. Niv. Behandelaar (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_f = [
    {
        "Description": "Testcase 09f (Wel ZVL functie + AOK OBPT + Kwal. Niv. Leerling (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_g = [
    {
        "Description": "Testcase 09g (Wel ZVL functie + AOK OBPT + Kwal. Overig zorgpersoneel (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Overig zorgpersoneel",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Vestiging_De_Beuk",
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_2_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Assertions
        test.verify_header_present('kwalificatie_niveau')
        test.verify_header_present('Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_bepaalde_tijd_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_oproepovereenkomsten_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_werkovereenkomsten_BBL_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_inhuurovereenkomsten_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_uitzendovereenkomsten_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_stageovereenkomsten_met_een_zorgverlener_functie')
        test.verify_header_present('Aantal_ingezette_uren_mbt_vrijwilligersovereenkomsten_met_een_zorgverlener_functie')
        test.verify_header_present('Totaal_aantal_ingezette_uren')
    finally:
        dg.delete_graph_data()


def test_if_number_of_rows_returned_is_correct_for_query_1_2_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Assertions
        test.verify_row_count(10)
    finally:
        dg.delete_graph_data()


def test_if_indicator_has_correct_value_for_query_1_2_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')


        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Assertions

        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    finally:
        dg.delete_graph_data()

def test_if_dates_can_change_1_2_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        test.set_reference_date_new_param_to("2024-05-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Assertions
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 5")]) 
    finally:
        dg.delete_graph_data()

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_2_3_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen WOK + Geen Kwal. Niv. - client (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None)) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_2_3_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen WOK + Geen Kwal. Niv. - client (Vest. 1287))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_1_2_3_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Geen WOK + Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None)) 
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_1_2_3_03(db_config):
    """ Testcase 03 (Geen ZVL functie + AOK + Geen Kwal. Niv. (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_2_3_04(db_config):
    """ Testcase 04 (Wel ZVL + Geen WOK + Geen Kwal. Niv. (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_ingezette_uren", (None,None)) 
        
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_1_2_3_05(db_config):
    """ Testcase 05 (Geen ZVL + AOK OBPT + Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        # test.verify_value_in_list("Totaal_aantal_ingezette_uren", "12",where_conditions=[("kwalificatie_niveau","Totaal_aantal_ingezette_uren aantal werknemers")) 
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_1_2_3_05_a(db_config):
    """ Testcase 05a (Geen ZVL + AOK OBPT + Kwal. Niv. 6 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        # test.verify_value_in_list("Totaal_aantal_ingezette_uren", "12",where_conditions=[("kwalificatie_niveau","Totaal_aantal_ingezette_uren aantal werknemers")) 
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_1_2_3_05_b(db_config):
    """ Testcase 05b (Geen ZVL + AOK BPT + Kwal. Niv. Leerling (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_ingezette_uren", (None,None)) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_1_2_3_06(db_config):
    """ Testcase 06 (Wel ZVL + Geen WOK + Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        # test.verify_value_in_list("Totaal_aantal_ingezette_uren", "12",where_conditions=[("kwalificatie_niveau","Totaal_aantal_ingezette_uren aantal werknemers")) 
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", (None,None))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_1_2_3_07(db_config):
    """ Testcase 07 (Wel ZVL + AOK OBPT + Geen Kwal. Niv. (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_ingezette_uren", (None,None)) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_1_2_3_08(db_config):
    """ Testcase 08 (Wel ZVL + AOK OBPT +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_1_2_3_08_a(db_config):
    """ Testcase 08a (Wel ZVL + AOK BPT +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_bepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_1_2_3_08_b(db_config):
    """ Testcase 08b (Wel ZVL + Oproep OK +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_oproepovereenkomsten_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08c
def test_if_value_returned_is_correct_for_query_1_2_3_08_c(db_config):
    """ Testcase 08c (Wel ZVL + AOK BBL +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_BBL_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_1_2_3_08_d(db_config):
    """ Testcase 08d (Wel ZVL + INH OK +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_inhuurovereenkomsten_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08e
def test_if_value_returned_is_correct_for_query_1_2_3_08_e(db_config):
    """ Testcase 08e (Wel ZVL + UITZ OK +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_uitzendovereenkomsten_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08f
def test_if_value_returned_is_correct_for_query_1_2_3_08_f(db_config):
    """ Testcase 08f (Wel ZVL + ST OK +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_stageovereenkomsten_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08g
def test_if_value_returned_is_correct_for_query_1_2_3_08_g(db_config):
    """ Testcase 08g (Wel ZVL + VW OK +  Kwal. Niv. 1 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_vrijwilligersovereenkomsten_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 1")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09
def test_if_value_returned_is_correct_for_query_1_2_3_09(db_config):
    """ Testcase 09 (Wel ZVL + AOK OBPT +  Kwal. Niv. 2 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 2")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09a
def test_if_value_returned_is_correct_for_query_1_2_3_09_a(db_config):
    """ Testcase 09a (Wel ZVL + AOK OBPT +  Kwal. Niv. 3 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0") ,where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 3")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09b
def test_if_value_returned_is_correct_for_query_1_2_3_09_b(db_config):
    """ Testcase 09b (Wel ZVL + AOK OBPT +  Kwal. Niv. 4 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 4")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09c
def test_if_value_returned_is_correct_for_query_1_2_3_09_c(db_config):
    """ Testcase 09c (Wel ZVL + AOK OBPT +  Kwal. Niv. 5 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 5")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09d
def test_if_value_returned_is_correct_for_query_1_2_3_09_d(db_config):
    """ Testcase 09d (Wel ZVL + AOK OBPT +  Kwal. Niv. 6 (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Kwalificatieniveau 6")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09e
def test_if_value_returned_is_correct_for_query_1_2_3_09_e(db_config):
    """ Testcase 09e (Wel ZVL + AOK OBPT +  Kwal. Niv. Behandelaar (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0")  ,where_conditions=[("kwalificatie_niveau","Behandelaar")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09f
def test_if_value_returned_is_correct_for_query_1_2_3_09_f(db_config):
    """ Testcase 09f (Wel ZVL + AOK OBPT +  Kwal. Niv. Leerling (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0") ,where_conditions=[("kwalificatie_niveau","Leerling")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09g
def test_if_value_returned_is_correct_for_query_1_2_3_09_g(db_config):
    """ Testcase 09g (Wel ZVL + AOK OBPT +  Kwal. Niv. Overig zorgpersoneel (Vest. 1254))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.2.3. Aantal ingezette uren aan werknemers met zorgverlenersfunctie per kwalificatieniveau per soort werkovereenkomst
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.2.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_new_param_to("2024-01-01")
        test.set_branch_number_new_param_to('"000001254"')

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_ingezette_uren_mbt_werkovereenkomsten_voor_onbepaalde_tijd_met_een_zorgverlener_functie", ("80","80.0"),where_conditions=[("kwalificatie_niveau","Overig zorgpersoneel")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()