from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-07-01

# Opmerkingen:
# testcases 02a & 08c geven errors terug. Dit zijn Query bugs (doorgevoerd op KIK-V Trello op 12-06-2024)

# Testcases:
td_template = [
    {
        "Description": "Testcase template (Human with nursing process and 4VG-G indication + vpt leveringsvorm)",
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["6VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]                        



td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_De_Beuk_1",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Locatie_Grotestraat_17",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Wlz-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Wel Wlz-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2023-01-01",
                        "end_date": "2024-06-30",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Wel Wlz-indicatie (<4VV))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wel Wlz-indicatie (4VG-G))",
        "Amount": 10, #Indicator score: 
                "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VG-G"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_d = [
    {
        "Description": "Testcase 02d (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_e = [
    {
        "Description": "Testcase 02e (Wel Zorgproces + Wmo-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen Zorgproces + Geen Wlz-indicatie + Wel Leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]
td_05 = [
    {
        "Description": "Testcase 05 (Geen Zorgproces + Wel Wlz-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel Zorgproces + Geen Wlz-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wel Zorgproces + Zvw-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt"]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wel Zorgproces + Wmo-indicatie + Wel leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel Zorgproces + Wel Wlz-indicatie (4VV) + Geen leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True, 
                        "ciz": ["4VV"], # "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [] #pgb, #vpt, mpt
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Wel Zorgproces + Wel Wlz-indicatie (5VV, zonder behandeling) + Geen leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": False,
                        "ciz": ["5VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel Zorgproces + Wel Wlz-indicatie (6VV) + VPT leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["6VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt"]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wel Zorgproces + Wel Wlz-indicatie (7VV) + MPT leveringsvorm))",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["7VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["mpt"]
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wel Zorgproces + Wel Wlz-indicatie (8VV) + PGB leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["8VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Wel Zorgproces + Wel Wlz-indicatie (9BVV) + PGB & MPT leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["9BVV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb", "mpt"]
                    }
                ]
            }
        ]
    }
]

td_08_d = [
    {
        "Description": "Testcase 08d (Wel Zorgproces + Wel Wlz-indicatie (10VV + Geen behandeling) + VPT & PGB leveringsvorm)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["10VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["vpt","pgb"]
                    }
                ]
            }
        ]
    }
]



#Static Tests
def test_if_headers_are_correct_for_query_1_1_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')
        test.set_reference_date_to("2024-01-01")
        test.set_branch_number_to("'000001287'")

        # Assertions
        test.verify_header_present('zorgprofiel')
        test.verify_header_present('verblijf_met_behandeling')
        test.verify_header_present('verblijf_zonder_behandeling')
        test.verify_header_present('pgb')
        test.verify_header_present('mpt')
        test.verify_header_present('vpt')
        test.verify_header_present('pgb_en_mpt')
        test.verify_header_present('aantal_uniek')
    finally:
        dg.delete_graph_data()

def test_if_number_of_rows_returned_is_correct_for_query_1_1_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm 
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')
        test.set_reference_date_to("2024-01-01")
        test.set_branch_number_to("'000001287'")

        # Assertions
        test.verify_row_count(1)
    finally:
        dg.delete_graph_data()

def test_if_indicator_has_correct_value_for_query_1_1_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        test.set_reference_date_to("2024-01-01")
        test.set_branch_number_to("'000001287'")

        # Assertions
        test.verify_value_in_list("aantal_uniek", ("1","1.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#6VV"),("vpt","1")])
    finally:
        dg.delete_graph_data()

def test_if_dates_can_change_1_1_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        test.set_reference_date_to("2023-01-01")
        test.set_branch_number_to("'000001254'")

        # Assertions
        test.verify_value_in_list("aantal_uniek", (None,None))
    finally:
        dg.delete_graph_data()

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_2_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_1_2_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_2_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_1_2_02_a(db_config):
    """ Testcase 02a (Geen Zorgproces + Wel Wlz-Indicatie (Niet op peildatum))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_1_1_2_02_b(db_config):
    """ Testcase 02b (Geen Zorgproces + Wel Wlz-Indicatie (<4VV))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_1_1_2_02_c(db_config):
    """ Testcase 02c (Geen Zorgproces + Wel Wlz-Indicatie (4VG-G))
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VG-G")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02d
def test_if_value_returned_is_correct_for_query_1_1_2_02_d(db_config):
    """ Testcase 02d (Wel Zorgproces + Zvw-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VG-G")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02e
def test_if_value_returned_is_correct_for_query_1_1_2_02_e(db_config):
    """ Testcase 02e (Wel Zorgproces + Wmo-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_2_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Wlz-Indicatie)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_1_2_03_a(db_config):
    """ Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-Indicatie + Geen leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_2_04(db_config):
    """ Testcase 04 (Geen Zorgproces + Geen Wlz-Indicatie + VPT leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_1_1_2_05(db_config):
    """ Testcase 05 (Geen Zorgproces + Wel Wlz-Indicatie + MPT leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_1_1_2_06(db_config):
    """ Testcase 06 (Wel Zorgproces + Geen Wlz-Indicatie + Wel leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_1_1_2_06_a(db_config):
    """ Testcase 06a (Wel Zorgproces + Zvw-Indicatie + Wel leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b
def test_if_value_returned_is_correct_for_query_1_1_2_06_b(db_config):
    """ Testcase 06b (Wel Zorgproces + Wmo-Indicatie + Wel leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", (None,None))#,where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_1_1_2_07(db_config):
    """ Testcase 07 (Wel Zorgproces + Wel Wlz-Indicatie (4VV) + Geen leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#4VV"),("verblijf_met_behandeling","10")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_1_1_2_07_a(db_config):
    """ Testcase 07a (Wel Zorgproces + Wel Wlz-Indicatie (5VV + Geen behandeling) + Geen leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query 
        test.verify_value_in_list("verblijf_zonder_behandeling", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#5VV"),("verblijf_zonder_behandeling","10")])
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_1_1_2_08(db_config):
    """ Testcase 08 (Wel Zorgproces + Wel Wlz-Indicatie (6VV) + VPT leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("vpt", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#6VV"),("aantal_uniek","10")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_1_1_2_08_a(db_config):
    """ Testcase 08a (Wel Zorgproces + Wel Wlz-Indicatie (7VV) + MPT leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query 
        test.verify_value_in_list("mpt", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#7VV"),("aantal_uniek","10"),("verblijf_met_behandeling","10")]) 
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_1_1_2_08_b(db_config):
    """ Testcase 08b (Wel Zorgproces + Wel Wlz-Indicatie (8VV) + PGB leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query        
        test.verify_value_in_list("pgb", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#8VV"),("aantal_uniek","10"),("verblijf_met_behandeling","10")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08c
def test_if_value_returned_is_correct_for_query_1_1_2_08_c(db_config):
    """ Testcase 08c (Wel Zorgproces + Wel Wlz-Indicatie (9BVV) + MPT & PGB leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("pgb_en_mpt", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#9BVV"),("aantal_uniek","10"),("verblijf_met_behandeling","10")]) 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_1_1_2_08_d(db_config):
    """ Testcase 08d (Wel Zorgproces + Wel Wlz-Indicatie (10VV) + BPT & PBG leveringsvorm)
        IGJ Contextinformatie t.b.v. inspectiebezoek 1.1.2 Aantal cliënten per zorgprofiel per leveringsvorm
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.2.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_uniek", ("10","10.0"),where_conditions=[("zorgprofiel","http://purl.org/ozo/onz-zorg#10VV"),("aantal_uniek","10"),("verblijf_met_behandeling","10")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()