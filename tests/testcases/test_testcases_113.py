from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-07-01

# Opmerkingen:
# testcases 02a & 08c geven errors terug. Dit zijn Query bugs (doorgevoerd op KIK-V Trello op 12-06-2024)

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen Zorgproces + Geen Zvw-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Zorgproces + Geen Zvw-indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel Zorgproces + Geen Zvw-indicatie)",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wel Zorgproces + Geen Zvw-indicatie (Wlz))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_b = [
    {
        "Description": "Testcase 02b (Wel Zorgproces + Geen Zvw-indicatie (Wmo))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WmoIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_02_c = [
    {
        "Description": "Testcase 02c (Wel Zorgproces + Wel Zvw-indicatie (Niet op peildatum))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-01",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen Zorgproces + Wel Zvw-indicatie)",
        "Amount": 10, 
        "Human": [
            {
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": []
                    }
                ]
            }
        ]
    }
]


td_03_a = [
    {
        "Description": "Testcase 03a (Wel Zorgproces (Niet op peildatum) + Wel Wlz-indicatie)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel Zorgproces + Zvw-indicatie)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": [""]
                    }
                ]
            }
        ],
        "Declaration": [
            {
                "approval": True,
                "date": "2024-07-01",
                "careperformancecode": "",
                "careproductcode": "", 
                "informationobjectvalue": "Test_Declaration_ID"
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel Zorgproces + Zvw-indicatie + GRZ-code)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": [""]
                    }
                ],
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2024-07-01",
                        "careperformancecode": "1001",
                        "careproductcode": "", 
                        "informationobjectvalue": "Test_Zvw__GRZ_Declaration_ID"
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel Zorgproces + Zvw-indicatie + ELV-code)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": [""]
                    }
                ],  
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2024-07-01",
                        "careperformancecode": "",
                        "careproductcode": "972800066", 
                        "informationobjectvalue": "Test_ELV_Declaration_ID"
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel Zorgproces + Zvw-indicatie + GRZ + ELV-code)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "ZvwIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""], 
                        "leveringsvorm": [""]
                    }
                ],
                "Declaration": [
                    {
                        "approval": True,
                        "date": "2024-07-01",
                        "careperformancecode": "1001",
                        "careproductcode": "972800066", 
                        "informationobjectvalue": "Test_GRZ_ELV_Declaration_ID"
                    }
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_1_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')
    test.set_reference_date_to("2023-01-01")
    test.set_branch_number_to("'000001254'")

    # Assertions
    test.verify_header_present('Aantal_clienten_ELV')
    test.verify_header_present('Aantal_clienten_GRZ')
    test.verify_header_present('Aantal_clienten_met_een_Zvw_indicatie_overig')
    test.verify_header_present('Totaal_aantal_unieke_clienten')

def test_if_number_of_rows_returned_is_correct_for_query_1_1_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig?  
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')
    test.set_reference_date_to("2023-01-01")
    test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_1_1_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

    test.set_reference_date_to("2022-07-01")
    test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("14","14.0"),where_conditions=[("Aantal_clienten_met_een_Zvw_indicatie_overig","14"),("Aantal_clienten_ELV","14"),("Aantal_clienten_GRZ","14")])

def test_if_dates_can_change_1_1_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

    test.set_reference_date_to("2023-07-01")
    test.set_branch_number_to('"000001254"')

    # Assertions
    test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("21","21.0"),where_conditions=[("Aantal_clienten_met_een_Zvw_indicatie_overig","21"),("Aantal_clienten_ELV","21"),("Aantal_clienten_GRZ","21")])

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_3_01(db_config):
    """ Testcase 01 (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_1_3_01_a(db_config):
    """ Testcase 01a (Geen Zorgproces + Geen Wlz-Indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_3_02(db_config):
    """ Testcase 02 (Wel Zorgproces + Geen Zvw-Indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_1_3_02_a(db_config):
    """ Testcase 02a (Wel Zorgproces + Geen Zvw-Indicatie (Wlz))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b
def test_if_value_returned_is_correct_for_query_1_1_3_02_b(db_config):
    """ Testcase 02b (Wel Zorgproces + Geen Zvw-Indicatie (Wmo))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02c
def test_if_value_returned_is_correct_for_query_1_1_3_02_c(db_config):
    """ Testcase 02c (Wel Zorgproces + Wel Zvw-indicatie (Niet op peildatum))
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()



# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_3_03(db_config):
    """ Testcase 03 (Geen Zorgproces + Wel Zvw-Indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_1_3_03_a(db_config):
    """ Testcase 03a (Geen Zorgproces (Niet op peildatum) + Wel Zvw-Indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("12","12.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_3_04(db_config):
    """ Testcase 04 (Wel Zorgproces + Zvw-indicatie)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_clienten_ELV",( "0","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])
        # test.verify_value_in_list("Aantal_clienten_GRZ",( "0","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])
        test.verify_value_in_list("Aantal_clienten_met_een_Zvw_indicatie_overig", ("22","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "22")])
        # test.verify_value_in_list("Totaal_aantal_unieke_clienten", ("10","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_1_1_3_04_a(db_config):
    """ Testcase 04a (Wel Zorgproces + Zvw-indicatie + ELV code)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_clienten_ELV", ("22","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "22")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_1_1_3_04_b(db_config):
    """ Testcase 04b (Wel Zorgproces + Zvw-indicatie + GRZ code)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        test.verify_value_in_list("Aantal_clienten_GRZ", ("22","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "22")])


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_1_1_3_04_c(db_config):
    """ Testcase 04c (Wel Zorgproces + Zvw-indicatie + ELV + GRZ code)
        IGJ contextinformatie t.b.v. Inspectiebezoek 1.1.3. Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig? 
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.3.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")
        test.set_branch_number_to("'000001287'")

        # Verify actual result of the query
        # test.verify_value_in_list("Aantal_clienten_ELV", ("10","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "12")])
        test.verify_value_in_list("Aantal_clienten_GRZ", ("22","22.0"),where_conditions=[("Totaal_aantal_unieke_clienten", "22"),("Aantal_clienten_ELV","22")])

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()