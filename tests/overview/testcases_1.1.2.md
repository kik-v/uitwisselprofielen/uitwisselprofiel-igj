---
title: Testcases IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.1.2 Wat is het aantal cliënten met een Wlz-indicatie per (combinatie van) leveringsvorm(en) per zorgprofiel?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.1.2 Wat is het aantal cliënten met een Wlz-indicatie per (combinatie van) leveringsvorm(en) per zorgprofiel?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft Zorgproces: N <br> Heeft Indicatie: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | AOK bij Vest. 1254 |
| 01a | td_01_a | Heeft Zorgproces: N <br> Heeft Indicatie: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | AOK bij Vest. 1287 |
| 02 | td_02 | Heeft Zorgproces: J <br> Heeft Indicatie: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten bij vest. 1287 |
| 02a | td_02_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (Wlz, niet op PD) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 02b | td_02_b | Heeft Zorgproces: J <br> Heeft Indicatie: J (1VV, Wlz <4VV) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 02c | td_02_c | Heeft Zorgproces: J <br> Heeft Indicatie: J (4VG-G) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 02d | td_02_d | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 02e | td_02_e | Heeft Zorgproces: J <br> Heeft Indicatie: J (Wmo) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 03 | td_03 | Heeft Zorgproces: N <br> Heeft Indicatie: J (4VV) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Clienten op vest. 1287 |
| 03a | td_03_a | Heeft Zorgproces: J (Niet op PD) <br> Heeft Indicatie: J (4VV) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 04 | td_04 | Heeft Zorgproces: N <br> Heeft Indicatie: N <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 05 | td_05 | Heeft Zorgproces: N <br> Heeft Indicatie: J (4VV) <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 06 | td_06 | Heeft Zorgproces: J <br> Heeft Indicatie: N <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 06a | td_06_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 06b | td_06_b | Heeft Zorgproces: J <br> Heeft Indicatie: J (Wmo) <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 07 | td_07 | Heeft Zorgproces: J <br> Heeft Indicatie: J (4VV) <br> Heeft leveringsvorm: N | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 07a | td_07_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (5VV, Zonder behandeling) <br> Heeft leveringsvorm: N | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 08 | td_08 | Heeft Zorgproces: J <br> Heeft Indicatie: J (6VV) <br> Heeft leveringsvorm: J (vpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 08a | td_08_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (7VV) <br> Heeft leveringsvorm: J (mpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 08b | td_08_b | Heeft Zorgproces: J <br> Heeft Indicatie: J (8VV) <br> Heeft leveringsvorm: J (pgb) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 08c | td_08_c | Heeft Zorgproces: J <br> Heeft Indicatie: J (9BVV) <br> Heeft leveringsvorm: J (pgb & mpt) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 08d | td_08_d | Heeft Zorgproces: J <br> Heeft Indicatie: J (10VV, zonder behandeling) <br> Heeft leveringsvorm: J (vpt & pgb) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |