---
title: Testcases IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.1.3 Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.1.3 Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| --- | --- | --- | --- |:---:|
| 01 | td_01 | Heeft Zorgproces: N <br> Heeft Indicatie: N | Peildatum: 2024-07-01 | AOK bij Vest. 1254 |
| 01a | td_01_a | Heeft Zorgproces: J <br> Heeft Indicatie: J | Peildatum: 2024-07-01 | AOK bij Vest. 1287 |
| 02 | td_02 | Heeft Zorgproces: J <br> Heeft Indicatie: N | Peildatum: 2024-07-01 | Clienten bij vest. 1287 |
| 02a | td_02_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (Wlz) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 02b | td_02_b | Heeft Zorgproces: J <br> Heeft Indicatie: J (Wmo) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 02c | td_02_c | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw, niet op PD) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 03 | td_03 | Heeft Zorgproces: N <br> Heeft Indicatie: J (Zvw) | Peildatum: 2024-07-01 | Clienten op vest. 1287 |
| 03a | td_03_a | Heeft Zorgproces: J (Niet op PD) <br> Heeft Indicatie: J (Zvw) | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | Clienten op vest. 1287 |
| 04 | td_04 | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) | Peildatum: 2024-07-01 | Clienten op Vest. 1287 |
| 04a | td_04_a | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) <br> Heeft Declaratie: J (GRZ) | Peildatum: 2024-07-01 | Clienten op Vest. 1287 |
| 04b | td_04_b | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) <br> Heeft Declaratie: J (ELV)| Peildatum: 2024-07-01 | Clienten op Vest. 1287 |
| 04c | td_04_c | Heeft Zorgproces: J <br> Heeft Indicatie: J (Zvw) <br> Heeft Declaratie: J (GRZ + ELV)| Peildatum: 2024-07-01 | Clienten op Vest. 1287 |