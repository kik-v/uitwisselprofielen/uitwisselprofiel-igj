---
title:  Testcases IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.4.1. Wat is het aantal personeelsleden in loondienst met een zorgverlener functie en het aantal personeelsleden niet in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.4.1. Wat is het aantal personeelsleden in loondienst met een zorgverlener functie en het aantal personeelsleden niet in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01a | td_01_a | Geen ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 02 | td_02 | Geen ZVL functie + Geen AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1254 |
| 03 | td_03 | Geen ZVL functie + Geen AOK + Kwal. Niv. 1 + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1254 |
| 04 | td_04 | Geen ZVL functie + Wel AOK (OBPT) + Geen Kwal. Niv. + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1254 |
| 05 | td_05 | Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 06 | td_06 | Geen ZVL functie + Geen AOK + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 07 | td_07 | Geen ZVL functie + Wel AOK (OBPT) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 08 | td_08 | Wel ZVL functie + Geen AOK (INH OK) + Geen Kwal. Niv. + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 09 | td_09 | Geen ZVL functie + Wel AOK (OBPT) + Kwal. Niv. 1 + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 11 | td_11 | Wel ZVL functie + Wel AOK OBPT + Geen Kwal. Niv. + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 12 | td_12 | Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 13 | td_13 | Wel ZVL functie + Geen AOK (INH OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 13a | td_13_a | Wel ZVL functie + Geen AOK (UITZ OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 13b | td_13_b | Wel ZVL functie + Geen AOK (ST OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 13c | td_13_c | Wel ZVL functie + Geen AOK (VW OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 14 | td_14 | Wel ZVL functie + Wel AOK + Geen Kwal. Niv. + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 15 | td_15 | Wel ZVL functie + Wel AOK + Kwal. Niv. 1 + Geen DAN dienst | Peildatum: 2024-01-01 | Vest. 1287 |
| 16a | td_16_a | Wel ZVL functie + Wel AOK BPT + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 16c | td_16_c | Wel ZVL functie + Geen AOK (AOK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 16d | td_16_d | Wel ZVL functie + Wel AOK (Oproep OK) + Kwal. Niv. 1 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 17 | td_17 | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 2 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 17b | td_17_b | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 4 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 17c | td_17_c | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 5 + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 17f | td_17_f | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. Leerling + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 17g | td_17_g | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. Overig Zorgpersoneel + Wel DAN dienst (D+A+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 18 | td_18 | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D) | Peildatum: 2024-01-01 | Vest. 1287 |
| 18a | td_18_a | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (A) | Peildatum: 2024-01-01 | Vest. 1287 |
| 18c | td_18_c | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+A) | Peildatum: 2024-01-01 | Vest. 1287 |
| 18d | td_18_d | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (D+N) | Peildatum: 2024-01-01 | Vest. 1287 |
| 18e | td_18_e | Wel ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 + Wel DAN dienst (A+N) | Peildatum: 2024-01-01 | Vest. 1287 |