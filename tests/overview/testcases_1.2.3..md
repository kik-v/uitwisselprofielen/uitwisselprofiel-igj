---
title:  Testcases IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.2.3. Wat is het aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel IGJ contextinformatie t.b.v. onaangekondigd inspectiebezoek 1.2.3. Wat is het aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?

## Testcases overview table ##


| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | Geen ZVL functie + Geen AOK + Geen Kwal. Niv. | Peildatum: 2024-01-01 | Vest. 1254|
| 01a | td_01_a | Geen ZVL functie + Geen AOK + Geen Kwal. Niv. | Peildatum: 2024-01-01 | Vest. 1287  |
| 02 | td_02 | Geen ZVL functie + Geen AOK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 03 | td_03 | Geen ZVL functie + Wel WOK + Geen Kwal. Niv. | Peildatum: 2024-01-01 | Vest. 125 |
| 04 | td_04 | Geen ZVL functie + Wel AOK + Geen Kwal. Niv. | Peildatum: 2024-01-01 | Vest. 1254  |
| 05 | td_05 | Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254 | 
| 05a | td_05_a | Geen ZVL functie + Wel AOK OBPT + Kwal. Niv. 6 | Peildatum: 2024-01-01 | Vest. 1254| 
| 05b | td_05_b | Geen ZVL functie + Wel AOK BPT + Kwal. Niv. Leerling | Peildatum: 2024-01-01 | Vest. 1254 |
| 06 | td_06 | Wel ZVL functie + Geen AOK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 07 | td_07 | Wel ZVL functie + Wel AOK OBPT + Geen Kwal. Niv. | Peildatum: 2024-01-01 | Vest. 1254  |
| 08 | td_08 | Wel ZVL functie + AOK OBPT + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254 |
| 08a | td_08_a | Wel ZVL functie + AOK BPT + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 08b | td_08_b | Wel ZVL functie + Oproep OK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 08c | td_08_c | Wel ZVL functie + AOK BBL + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 08d | td_08_d | Wel ZVL functie + INH OK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254 |
| 08e | td_08_e | Wel ZVL functie + UITZ OK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 08f | td_08_f | Wel ZVL functie + ST OK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 08g | td_08_g | Wel ZVL functie + VWOK + Kwal. Niv. 1 | Peildatum: 2024-01-01 | Vest. 1254  |
| 09 | td_09 | Wel ZVL functie + AOK OBPT + Kwal. Niv. 2 | Peildatum: 2024-01-01 | Vest. 1254  |
| 09a | td_09_a | Wel ZVL functie + AOK OBPT + Kwal. Niv. 3 | Peildatum: 2024-01-01 | Vest. 1254 |
| 09b | td_09_b | Wel ZVL functie + AOK OBPT + Kwal. Niv. 4 | Peildatum: 2024-01-01 | Vest. 1254  |
| 09c | td_09_c | Wel ZVL functie + AOK OBPT + Kwal. Niv. 5 | Peildatum: 2024-01-01 | Vest. 1254  |
| 09d | td_09_d | Wel ZVL functie + AOK OBPT + Kwal. Niv. 6 | Peildatum: 2024-01-01 | Vest. 1254  |
| 09e | td_09_e | Wel ZVL functie + AOK OBPT + Kwal. Niv. Behandelaar | Peildatum: 2024-01-01 | Vest. 1254  |
| 09f | td_09_f | Wel ZVL functie + AOK OBPT + Kwal. Niv. Leerling | Peildatum: 2024-01-01 | Vest. 1254 |
| 09g | td_09_g | Wel ZVL functie + AOK OBPT + Kwal. Overig zorgpersoneel | Peildatum: 2024-01-01 | Vest. 1254  |