
#  Testcases Overzicht IGJ contextinformatie t.b.v onaangekondigd inspectiebezoek #
---

Hieronder staat een overzicht van de status van de testautomatisering voor IGJ contextinformatie t.b.v onaangekondigd inspectiebezoek.

## Status ##

| Indicator           | Specificatie | Data         | Testautomatisering | Opmerking |
|---------------------|--------------|--------------|----------------|-----------|
| Indicator 1.1.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.2.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.1.3.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.2.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.2.3.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.4.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 1.4.2.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |