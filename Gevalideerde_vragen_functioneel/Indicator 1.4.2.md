---
title: 1.4.2. Wat is het aantal ingezette uren aan personeel in loondienst met zorgverlener functies per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)?
description: "Aantal ingezette uren aan personeel in loondienst met zorgverlener functies per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum." 
weight: 7
---

## Indicator

**Definitie:** Aantal ingezette uren aan personeel in loondienst met zorgverlener functies per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum.

**Teller:** Aantal ingezette uren aan personeel in loondienst met zorgverlener functies per kwalificatieniveau per DAN-dienst.

**Noemer:** Niet van toepassing.


## Toelichting

Deze indicator betreft het aantal ingezette uren aan personeel in loondienst met zorgverlener functies per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum. De indicator wordt voor een vestiging per kwalificatieniveau en voor alle kwalificatieniveaus samen berekend.


## Uitgangspunten

* Alle personeelsleden in loondienst met een zorgverlener functie op de peildatum worden geïncludeerd.
* Of een personeelslid in loondienst een zorgverlener functie heeft, wordt bepaald op basis van de functie in de werkovereenkomst.
* DAN-dienst: Een dagdienst (D) vindt plaats van 7.00 - 15.59 uur, een avonddienst (A) van 16.00 - 22.59 en een nachtdienst (N) van 23.00 - 6.59 uur.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer van de betreffende vestiging op de peildatum alle personeelsleden in loondienst met een of meerdere zorgverlener functie(s).
2. Bepaal op basis van de functie(s) per personeelslid in loondienst uit stap 1 het kwalificatieniveau en het aantal ingezette uren per dienst (Dag-Avond-nacht).
3. Bereken op basis van stap 2 per kwalificatieniveau het totaal aantal ingezette uren aan personeel in loondienst per DAN-dienst.
4. Bereken op basis van stap 3 het totaal aantal ingezette uren aan personeel in loondienst per DAN-dienst.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Kwalificatieniveau | Aantal ingezette uren aan PIL met een zorgfunctie in de dagdienst | Aantal ingezette uren aan PIL met een zorgfunctie in de avonddienst | Aantal ingezette uren in de nachtdienst |
|---|---|---|---|
| 0/1 | Stap 3 | Stap 3 | Stap 3 | 
| 2 | Stap 3 | Stap 3 | Stap 3 | 
| N | Stap 3 | Stap 3 | Stap 3 | 
| Totaal aantal ingezette uren | Stap 4 | Stap 4 | Stap 4 | 
