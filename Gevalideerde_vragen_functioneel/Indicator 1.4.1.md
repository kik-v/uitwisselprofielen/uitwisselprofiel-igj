---
title: 1.4.1. Wat is het aantal personeelsleden in loondienst met een zorgverlener functie en het aantal personeelsleden niet in loondienst met een zorgverlener functie per kwalificatieniveau per Dag-, Avond- en Nacht-dienst (DAN-dienst)?
description: "Aantal personeelsleden in loondienst met een zorgverlener functie en niet in loondienst met een zorgverlener functie per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum." 
weight: 6
---

## Indicator

**Definitie:** Aantal personeelsleden in loondienst met een zorgverlener functie en niet in loondienst met een zorgverlener functie per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum.

**Teller:** Aantal personeelsleden in loondienst met een zorgverlener functie en niet in loondienst met een zorgverlener functie per kwalificatieniveau per DAN-dienst.

**Noemer:** Niet van toepassing.


## Toelichting

Deze indicator betreft het aantal personeelsleden, zowel in loondienst als niet in loondienst, met een zorgverlener functies per kwalificatieniveau per DAN-dienst bij een vestiging op een peildatum. De indicator wordt voor een vestiging per kwalificatieniveau en voor alle kwalificatieniveaus samen berekend.


## Uitgangspunten

* Alle personeelsleden in loondienst met (een of meerdere) zorgverlener functie(s) op de peildatum worden geïncludeerd.
* Alle personeelsleden niet in loondienst met (een of meerdere) zorgverlener functie(s) op de peildatum worden geïncludeerd.
* Of een personeelslid in loondienst een zorgverlener functie heeft, wordt bepaald op basis van de functie in de werkovereenkomst.
* DAN-dienst: Een dagdienst (D) vindt plaats van 7.00 - 14.59 uur, een avond-dienst (A) van 15.00 - 22.59 en een nacht-dienst (N) van 23.00 - 6.59 uur.
* De indeling van een personeelslid in een dienst wordt bepaald op basis van de starttijd van het ingezette personeelslid.
* De definities van personeel in loondienst en personeel niet in loondienste staan beschreven in de Algemene Uitgangspunten: https://gitlab.com/kik-v/uitwisselprofielen/algemene-uitgangspunten/-/blob/dev/Documentatie/_index.md?ref_type=heads#relaties-tussen-werkovereenkomsten-rollen-en-groepen


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer van de betreffende vestiging op de peildatum alle personeelsleden in loondienst met een of meerdere zorgverlener functie(s) en alle personeelsleden niet in loondienst met een of meerdere zorgverlener functie(s).
2. Bepaal op basis van de functie(s) per personeelslid uit stap 1 het kwalificatieniveau en de starttijd van de inzet. het aantal ingeplande uren per dienst (Dag-Avond-nacht).
3. Bepaal op basis van stap 2 (op basis van de starttijd van de inzet) binnen welke dienst (D-A-N) het personeelslid is ingezet.
4. Bereken op basis van stap 3 het per kwalificatieniveau het aantal personeelsleden in loondienst per DAN-dienst en het aantal personeelsleden niet in loondienst per DAN-dienst.
5. Bereken op basis van stap 4 het totaal aantal personeelsleden per DAN-dienst.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Kwalificatieniveau | Aantal PIL met een zorgfunctie in de dagdienst | Aantal PIL met een zorgfunctie in de avonddienst | Aantal PIL met een zorgfunctie in de nachtdienst | Aantal PNIL met een zorgfunctie in de dagdienst | Aantal PNIL met een zorgfunctie in de avonddienst | Aantal PNIL met een zorgfunctie in de nachtdienst |
|---|---|---|---|---|---|---|
| 0/1 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| 2 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| N | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| Totaal | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | 

