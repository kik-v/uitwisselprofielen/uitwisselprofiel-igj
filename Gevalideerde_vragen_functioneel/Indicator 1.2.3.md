---
title: 1.2.3. Wat is het aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?
description: "Aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum." 
weight: 5
---

## Indicator

**Definitie:** Aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum.

**Teller:** Aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal ingezette uren (en het percentage t.o.v. totaal) aan medewerkers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum. De indicator wordt per kwalificatieniveau en voor alle kwalificatieniveaus samen berekend.


## Uitgangspunten

* Alle medewerkers met een of meerdere zorgverlener functies op de peildatum worden geïncludeerd.
* Of een werknemer een zorgverlener functie heeft, wordt bepaald op basis van de functie in de werkovereenkomst.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer van de betreffende vestiging op de peildatum van alle medewerkers de werkovereenkomsten met een zorgverlener functie.
2. Bepaal op basis van de functie per werkovereenkomst uit stap 1 het kwalificatieniveau.
3. Bereken op basis van stap 2 per kwalificatieniveau het totaal aan ingezette uren en per kwalificatieniveau het totaal aan ingezette uren per soort werkovereenkomst.
4. Bereken op basis van stap 3 het totaal aan ingezette uren per soort werkovereenkomst.
5. Bereken op basis van stap 3 het totaal aan ingezette medewerkers per soort werkovereenkomst.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Kwalificatieniveau | Aantal ingezette uren m.b.t. werkovereenkomsten voor onbepaalde tijd met een zorgverlener functie | Aantal ingezette uren m.b.t. werkovereenkomsten voor bepaalde tijd met een zorgverlener functie | Aantal ingezette uren m.b.t. oproepovereenkomsten met een zorgverlener functie | Aantal ingezette uren m.b.t. werkovereenkomsten BBL met een zorgverlener functie | Aantal ingezette uren m.b.t. inhuurovereenkomsten met een zorgverlenerfunctie | Aantal ingezette uren m.b.t. uitzendovereenkomsten met een zorgverlenerfunctie | Aantal ingezette uren m.b.t. stageovereenkomsten met een zorgverlenerfunctie |  Aantal ingezette uren m.b.t. vrijwilligersovereenkomsten met een zorgverlenerfunctie | Totaal aantal werkovereenkomsten | 
|---|---|---|---|---|---|---|---|---|---|
| 0/1 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 |
| 2 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| N | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| Totaal aantal ingezette uren | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| Totaal aantal ingezette medewerkers | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | 
