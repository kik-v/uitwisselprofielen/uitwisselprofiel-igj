---
title:  1.1.1. Wat is het aantal cliënten per wet?
weight: 1
---

## Indicator

**Definitie:** Aantal cliënten op basis van de Wlz-indicatie en/of een traject gebaseerd op de Zvw bij een vestiging op een peildatum.

**Teller:** Aantal cliënten per wet.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal cliënten op basis van de Wlz-indicatie en/of een traject gebaseerd op de Zvw bij een vestiging op een peildatum. 

## Uitgangspunten

* Cliënten die op de peildatum beschikten over zowel een een traject gebaseerd op de Zvw als een Wlz-indicatie worden in zowel de Wlz als de Zvw meegeteld.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvingen op de betreffende vestiging. 
2. Bepaal op basis van stap 1 op basis van de indicatie en/of het traject gebaseerd op de Zvw of de cliënt in de Wlz of de Zvw valt. 
3. Bereken op basis van stap 2 het aantal cliënten per wet een Wlz-indicatie, een traject gebaseerd op de Zvw en het aantal overige cliënten.
4. Bereken op basis van stap 3 het totaal aantal unieke cliënten.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Aantal cliënten met een Wlz-indicatie | Aantal cliënten met een traject gebaseerd op de Zvw | Aantal cliënten Overig | Totaal aantal unieke cliënten |
|---|---|---|---|
| Stap 2 | Stap 2 | Stap 3 | Stap 4 |
