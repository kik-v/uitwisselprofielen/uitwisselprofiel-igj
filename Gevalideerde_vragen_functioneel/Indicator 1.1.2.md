---
title: 1.1.2 Wat is het aantal cliënten met een Wlz-indicatie per (combinatie van) leveringsvorm(en) per zorgprofiel?
weight: 2
---

## Indicator

**Definitie:** Aantal cliënten met een Wlz-indicatie per (combinatie van) leveringsvorm(en) per zorgprofiel bij een vestiging op een peildatum. 

**Teller:** Aantal cliënten per (combinatie van) leveringsvorm(en) per zorgprofiel.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal cliënten per (combinatie van) leveringsvorm(en) per zorgprofiel bij een vestiging op een peildatum. Met betrekking tot de leveringsvorm verblijf wordt tevens aangegeven of dit met of zonder behandeling is.


## Uitgangspunten

* Cliënten die op de peildatum beschikten over meerdere leveringsvormen en/of meerdere zorgprofielen worden in alle desbetreffende zorgprofielen en/of leveringsvormen ingedeeld. 


## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvingen op de betreffende vestiging en beschikten over een Wlz-indicatie.
2. Bepaal op basis van stap 1 het zorgprofiel en de leveringsvorm(en) waaronder de Wlz-indicatie(s) vallen en of dit met of zonder behandeling is. 
3. Bereken op basis van stap 2 het aantal cliënten per zorgprofiel per leveringsvorm en combinatie van MPT en PGB per zorgprofiel.
4. Bereken op basis van stap 2 het aantal unieke cliënten per zorgprofiel, per leveringsvorm en het totaal.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Zorgprofiel | Verblijf met behandeling |  Verblijf zonder behandeling | PGB | VPT | MPT | Zowel PGB als MPT | Totaal aantal unieke cliënten |
|---|---|---|---|---|---|---|---|
| VV4 | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 4 | 
| VV... | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing | Stap 3 |Stap 3 | Stap 4 | 
| VV9b | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 4 | 
| LG 1 | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 4 | 
| LG... | Stap 3 | Stap 3 | Stap 3 | Niet van toepassing | Stap 3 | Stap 3 | Stap 4 | 
| Totaal aantal unieke cliënten  | Stap 4 | Stap 4 | Stap 4 | Niet van toepassing | Stap 4 | Stap 4 | Stap 4 | 

