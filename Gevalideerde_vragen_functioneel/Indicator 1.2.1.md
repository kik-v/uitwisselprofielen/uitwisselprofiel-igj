---
title: 1.2.1. Wat is het aantal medewerkers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?
description: "Aantal medewerkers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum." 
weight: 4
---

## Indicator

**Definitie:** Aantal medewerkers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum.

**Teller:** Aantal medewerkers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal medewerkers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een vestiging op een peildatum. De indicator wordt per kwalificatieniveau en voor alle kwalificatieniveaus samen berekend.


## Uitgangspunten

* Alle medewerkers met een of meerdere zorgverlener functies op de peildatum worden geïncludeerd.
* Of een medewerker een zorgverlener functie heeft, wordt bepaald op basis van de functie in de werkovereenkomst.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer van de betreffende vestiging op de peildatum van alle medewerkers de werkovereenkomsten met een zorgverlener functie.
2. Bepaal op basis van de functie per werkovereenkomst uit stap 1 het kwalificatieniveau.
3. Bereken op basis van stap 2 per kwalificatieniveau het totaal aantal medewerkers en per kwalificatieniveau het aantal medewerkers per soort werkovereenkomst.
4. Bereken op basis van stap 3 het totaal aantal medewerkers per soort werkovereenkomst.
5. Bereken het percentage aan medewerkers per soort werkovereenkomst.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Kwalificatieniveau | Aantal medewerkers met een werkovereenkomst voor onbepaalde tijd met een zorgverlener functie | Aantal medewerkers met een werkovereenkomst voor bepaalde tijd met een zorgverlener functie | Aantal medewerkers met een oproepovereenkomst met een zorgverlener functie | Aantal medewerkers met een werkovereenkomst BBL met een zorgverlener functie | Aantal medewerkers met een inhuurovereenkomst met een zorgverlenerfunctie | Aantal medewerkers met een uitzendovereenkomst met een zorgverlenerfunctie | Aantal medewerkers met een stageovereenkomst met een zorgverlenerfunctie | Aantal medewerkers met een vrijwilligersovereenkomst met een zorgverlenerfunctie | Totaal |
|---|---|---|---|---|---|---|---|---|---|
| 0/1 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| 2 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| N | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| Totaal aantal medewerkers | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| % totaal aantal medewerkers | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | 100% | 
