---
title: 1.1.3 Wat is het aantal cliënten met een Zvw-indicatie met ELV, GRZ en overig?
weight: 3
---

## Indicator

**Definitie:** Aantal cliënten met een Zvw-indicatie met ELV en/of GRZ en overig bij een vestiging op een peildatum. 

**Teller:** Aantal cliënten.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal cliënten met een Zvw-indicatie met ELV en/of GRZ en overig bij een vestiging op een peildatum.

## Uitgangspunten
* De indeling van een cliënt met een Zvw-indicatie in ELV en GRZ wordt bepaald op basis van de productcode (ook wel prestatiecode) die in het ECD geregistreerd staat bij de Zvw-indicatie van de cliënt. Op basis van deze productcode zal de geleverde zorg vervolgens gedeclareerd worden. De prestatiecodes op basis waarvan deze indeling (in ELV en GRZ) plaatsvindt staat nader beschreven in de Algemene uitgangspunten in de paragraaf 'Het gebruik van declaraties'.  

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Zvw-indicatie op de betreffende vestiging. 
2. Bereken op basis van stap 1 het aantal cliënten per Zvw-indicatie met GRZ en ELV en het aantal overige cliënten met een Zvw-indicatie.
3. Bereken op basis van stap 2 het totaal aantal unieke cliënten.

Vestiging: Naam

Peildatum: dd-mm-jjjj

| Aantal cliënten ELV | Aantal cliënten GRZ | Aantal cliënten met een Zvw-indicatie overig | Totaal aantal unieke cliënten |
|---|---|---|---|
| Stap 2 | Stap 2 | Stap 2 | Stap 3 |
