---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.1.0-RC4-acc-test |  
| Versie | 1.1.0 vastgesteld door de Ketenraad KIK-V. |
| Doel | Release 1.1.0 betreft het uitwisselprofiel van de IGJ met contextinformatie ten behoeve van het inspectiebezoek |
| Doelgroep | Inspectie Gezondheidszorg en Jeugd Zorgaanbieders verpleging en verzorging (V&V) |
| Totstandkoming | De ontwikkeling van release 1.1.0 is uitgevoerd door het programma KIK-V in afstemming met een werkgroep/afvaardiging vanuit de IGJ op basis van een interne evaluatie bij de IGJ. Release 1.1.0 wordt vastgesteld door de Ketenraad KIK-V. |
| Operationeel toepassingsgebied | Aanlevering contextinformatie t.b.v. het inspectiebezoek |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 1.1.0 omvat de volledige uitvraag naar contextinformatie t.b.v. het inspectiebezoek. |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |

