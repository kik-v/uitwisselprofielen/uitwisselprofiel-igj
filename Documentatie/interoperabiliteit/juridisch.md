---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag
Dit uitwisselprofiel kent zijn grondslag in de volgende wetsartikelen:

* Wet kwaliteit, klachten en geschillen zorg (Wkkgz): artikel 11
* Algemene wet bestuursrecht (Awb): artikel 5:16, 5:17, 5:20
* Wet zorg en dwang (Wzd): artikel 17, 18, 42 en 60a

Lees hiervoor ook de aanvullende toelichting op het juridisch kader binnen de afsprakenset via onderstaande link. 
[Juridisch kader afsprakenset](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader) 