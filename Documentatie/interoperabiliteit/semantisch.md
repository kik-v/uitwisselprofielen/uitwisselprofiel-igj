---
title: Semantische interoperabiliteit
weight: 3
---
## Identificerende vragen

| Informatievraag | Aggregatieniveau |
| --- | --- |
| Wat is de naam van de organisatie? | Organisatieniveau |
| Wat is het KvK-nummer van de organisatie? | Organisatieniveau |
| Wat is de naam van de vestiging? | Vestigingsniveau |
| Wat is het KvK-vestigingsnummer? | Vestigingsniveau |

### Doel en achtergrond
De IGJ vraagt deze gegevens om te verifiëren of de gegevens inderdaad horen bij de zorgaanbieder die zij bezocht heeft.

## Informatievragen
#### 1.1.1. Wat is het aantal cliënten per wet?
| Doel en achtergrond | Aggregatieniveau |
|-------|-------|
| Voorafgaand aan een bezoek vraagt de IGJ een actueel beeld van het aantal cliënten die wonen op een vestiging op basis van een Wlz-indicatie en/of een Zvw-traject en/of andere cliënten ("overig"). Hiermee heeft de inspectie een eerste beeld van wat de grondslag (indicatie) van de zorg is en een beeld van het totaal aantal cliënten op de vestiging. | Vestigingsniveau |

#### 1.1.2. Wat is het aantal cliënten met een Wlz-indicatie per (combinatie van) leveringsvorm(en) per zorgprofiel VV?

#### 1.1.3. Wat is het aantal cliënten met een Zvw-traject met ELV, GRZ of een overig Zvw-traject?
| Doel en achtergrond | Aggregatieniveau |
|-------|-------|
| De IGJ vraagt om een actueel beeld van de verschillende leveringsvormen van de zorg die cliënten op een vestiging ontvangen. Hiermee heeft de inspectie een eerste beeld van wat de leveringsvorm van de zorg is. Ook geeft dit een eerste beeld van waar de medische eindverantwoordelijkheid ligt (bij de huisarts of specialist ouderengeneeskunde). | Vestigingsniveau |

#### 1.2.1. Wat is het aantal (en percentage t.o.v. totaal) werknemers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?

#### 1.2.3. Wat is het aantal (en percentage t.o.v. totaal) ingezette uren aan werknemers met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?
| Doel en achtergrond | Aggregatieniveau |
|-------|-------|
| De IGJ wil de samenstelling van kwalificatieniveaus in een team van zorgverleners af kunnen zetten tegen de zorgvraag van cliënten op een vestiging. Zo kan worden ingeschat of de kennis en kunde van zorgverleners op een vestiging passend is bij de zorgvraag van cliënten. Hoe de samenstelling (in harde cijfers) van een zorgteam eruit moet zien, wordt niet vanuit de IGJ bepaald maar is juist onderwerp van gesprek. De IGJ biedt, in het kader van de krapte op de arbeidsmarkt, ruimte voor het anders organiseren van zorg, mits dit veilig, verantwoord en onderbouwd is. Dit geldt ook voor inzet van andersoortig personeel. | Vestigingsniveau |

#### 1.4.1. Wat is het aantal personeelsleden in loondienst met een zorgverlener functie per kwalificatieniveau per dag-avond-nacht-dienst?

#### 1.4.2. Wat is het aantal ingezette uren aan werknemers met een zorgverlener functie per kwalificatieniveau per dag-avond-nacht-dienst?
| Doel en achtergrond | Aggregatieniveau |
|-------|-------|
| De IGJ wil zicht krijgen op het kwalificatieniveau van de zorgverleners die gedurende de dag, avond en nacht ingezet worden. Het aantal personeelsleden per dienst wordt bepaald aan de hand van de starttijd van de dienst: 7.00 uur, 15.00 uur en 23.00 uur. | Vestigingsniveau |

## Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. 

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)

## Benodigde gegevens
De concepten, eigenschappen en relaties die nodig zijn om de informatievragen IGJ te beantwoorden staan: [klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset/).

## Berekeningen
Berekening van de informatievragen vindt plaats onder verantwoordelijkheid van de zorgaanbieder. De zorgaanbieder is verantwoordelijk voor de aggregatie naar het gewenste niveau.

Klik [hier](/Gevalideerde_vragen_functioneel/) voor de (voorbeeld)berekeningen informatievragen IGJ.

## Eisen aan actualiteit, betrouwbaarheid en volledigheid van data
Het is voor de inspectie belangrijk dat de inspectie ten tijde van een inspectiebezoek beschikt over actuele gegevens. Dit is ook in het belang van de zorgaanbieder zelf. Als bijvoorbeeld wordt aangegeven dat de zorgaanbieder cliënten heeft met een zorgprofiel VV5, maar hier niet het juiste personeel voor heeft, wordt dit als risico gezien. In de praktijk kan het zo zijn dat die aanbieder op het moment van bezoek al geen zorg meer verleent aan cliënten met zorgprofiel VV5.

## Aggregatieniveau
Het aggregatieniveau van gegevens t.b.v. het inspectiebezoek is voor alle informatievragen **vestigingsniveau.**

## Contextinformatie
Tijdens het inspectiebezoek kan context / toelichting worden gegeven in het geval van afwijkingen / afwijkende cijfers. Daarnaast kunnen ook verbeterplannen en kwaliteitsplannen en -verslagen voor contextinformatie worden gebruikt.
