---
title: Technische interoperabiliteit
weight: 4
---
In het geval van een aangekondigd inspectiebezoek wordt per e-mail een aankondigingsbrief verstuurd aan de bestuurder van een zorgorganisatie. In geval van een onaangekondigd bezoek meldt de IGJ zich bij de receptie van de te bezoeken vestiging. Een email over het inspectiebezoek wat gaande is op de betreffende vestiging, met het verzoek om contextinformatie aan te leveren, wordt tijdens het bezoek overlegd en verzonden aan de raad van bestuur. Afhankelijk van de technische mogelijkheden van zowel de IGJ als zorgaanbieders zijn er twee situaties mogelijk:

1. De zorgaanbieder kan indicatoren berekenen en aanleveren via een datastation: de vragen van de IGJ worden via de KIK-Starter aan de zorgaanbieder gesteld. Vervolgens worden de indicatoren via de KIK-Starter berekend en de antwoorden worden via de KIK-Starter aangeleverd aan de IGJ. Voor meer informatie hierover zie [het Publicatieplatform](https://www.kik-v-publicatieplatform.nl).
2. De zorgaanbieder kan zelf indicatoren berekenen en aanleveren via Excel: de vragen van de IGJ worden in een Excel-bestand als bijlage bij de e-mail met aankondigingsbrief toegevoegd. Vervolgens stuurt de zorgaanbieder de antwoorden op dezelfde manier terug. Terugkoppeling aan de zorgaanbieder vindt plaats middels het inspectierapport.