---
title: Organisatorische interoperabiliteit
weight: 2
---
## Vorm van terugkoppeling
De IGJ koppelt de bevindingen en een oordeel over de vestiging terug in het inspectierapport. Het aantal cliënten, type cliënten en de aantallen en deskundigheidsniveaus van zorgverleners wordt standaard opgenomen. Er wordt namelijk gekeken of het aantal en het deskundigheidsniveau van zorgverleners passend is bij het aantal cliënten en de zorgvraag van de cliënten. Indien er feitelijke onjuistheden in het rapport zijn opgenomen, kan de zorgaanbieder dit schriftelijk aangeven.

## Moment van aanlevering
In het geval van een aangekondigd inspectiebezoek kunnen de gegevens worden aangeleverd tot een week voorafgaand aan het bezoek. In geval van een onaangekondigd inspectiebezoek vraagt de inspectie de zorgaanbieder de contextinformatie op de dag van het bezoek aan te leveren of uiterlijk 24 uur daarna. De IGJ kan tijdens een inspectiebezoek altijd (aanvullende) vragen stellen over deze en andere informatie.

## Terugkoppeling
Er vind geen aparte terugkoppeling naar aanleiding van de aangeleverde contextinformatie plaats, deze wordt opgenomen in het inspectierapport. De manier waarop de IGJ rapporten openbaar maakt, is te vinden op de website: https://www.igj.nl/onderwerpen/openbaarmaking

## Peildata of -periode
Voor de aanlevering van contextinformatie t.b.v. het inspectiebezoek zijn de peildatum (de datum waarop de gegevens gebaseerd worden) en het meetmoment (het moment waarop de indicator berekend wordt) gelijk aan elkaar. Het betreft hier bij zowel een aangekondigd als een onaagekondigd bezoek de dag van het inspectiebezoek.

## Bewaartermijn
De antwoorden die worden gegeven dienen als context bij het inspectiebezoek en de achterliggende data die hiervoor wordt gebruikt hoeft niet bewaard te worden door de zorgaanbieder. De uitvraag wordt gezien als een momentopname. Er geldt dus geen bewaartermijn. De IGJ verwerkt de aangeleverde antwoorden in het inspectierapport dat na afloop van een bezoek wordt opgesteld en gepubliceerd.

## Afspraken over nalevering of correctie
Op het moment dat een zorgaanbieder de gegevens niet tijdig kan aanleveren of een correctie wil doorvoeren in de aangeleverde gegevens, kan de zorgaanbieder contact opnemen met de IGJ.

## In- en exclusiecriteria -bepalen van de doelgroep
De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen:

* Sector (type)
  * Inclusie: Verpleging en verzorging (intramurale zorg) en locaties waar scheiden van wonen en zorg van toepassing is;
  * Exclusie: Thuiszorg in de wijk (extramurale zorg), Gehandicaptenzorg, GGZ en ‘Geen zorg’
* Zorgprofiel (inclusiecriteria)
  * VV04 Beschut wonen met intensieve begeleiding en uitgebreid wonen
  * VV05 Beschermd wonen met intensieve dementiezorg
  * VV06 Beschermd wonen met intensieve verzorging en verpleging
  * VV07 Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding
  * VV08 Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging/ verpleging
  * VV09B Herstelgerichte behandeling met verpleging en verzorging
  * VV10 / PTZ Beschermd verblijf met intensief palliatief-terminale zorg

## In- en exclusiecriteria
Het is aan de inspecteur om, voorafgaand aan (of tijdens) het bezoek, te bepalen welk toetsingskader gehanteerd wordt. Als het toetsingskader V&V wordt gebruikt, dan wordt alleen gekeken naar de cliënten V&V en niet naar de cliënten die vallen onder andere sectoren en/of wetten. Voor medewerkers geldt dat alle zorgmedewerkers van de vestiging worden geïncludeerd.
