---
title: Algemeen
weight: 2
---
# Algemeen
## Doel van de uitvraag
De IGJ wil als context bij het inspectiebezoek inzicht krijgen in:

* De cliëntenpopulatie op een vestiging;
* Het aantal en de deskundigheid van zorgverleners op een vestiging.

De opgevraagde gegevens worden als contextinformatie opgenomen in het uiteindelijke inspectierapport.

Het doel en de achtergrond per informatievraag wordt toegelicht bij de semantische laag.