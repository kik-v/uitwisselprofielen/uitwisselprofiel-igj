---
title: Introductie
weight: 1
---

Let op! Het uitwisselprofiel zal een nieuwe naam krijgen, namelijk "Uitwisselprofiel IGJ Contextinformatie t.b.v. inspectiebezoek". In verschillende teksten is dit al aangepast. De aanpassing in de naam zoals hierboven weergegeven en in de link volgen bij de halfjaarlijkse release (juli 2024).

Het Uitwisselprofiel IGJ Contextinformatie t.b.v. inspectiebezoek bestaat uit drie secties:
* de algemene documentatie, bestaande uit een sectie: [algemeen](/Documentatie/algemeen/), [release-informatie](/Documentatie/release-info/), [interoperabiliteit](/Documentatie/interoperabiliteit/) en [privacy & informatiebeveiliging](/Documentatie/privacy%26informatiebeveiliging/);
* de [functionele berekeningen](/Gevalideerde_vragen_functioneel/) van de gevalideerde vragen;
* de [technische uitwerking](/Gevalideerde_vragen_technisch/) van de gevalideerde vragen in de vorm van SPARQL's.