def indicator_igj_1_2_3(peildatum, vestiging, mensen):
    gewerkte_uren = 0
    indicator = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            werkovereenkomst_type = werkovereenkomst.get_rdftype()
            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                functie = werkovereenkomstafspraak.get_functie()
                if functie in ZorgFuncties:
                    functie_niveau = FunctieNiveaus[functie]
                    afspraak_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(peildatum, afspraak_periode):
                        for gewerkte_periode in gewerkte_perioden:
                            uren = gewerkte_periode.get_uren()
                            dag = gewerkte_periode.get_date()
                            if dag == peildatum and (gewerkte_periode.get_locatie() == vestiging or not vestiging):
                                gewerkte_uren += uren
                                if functie_niveau not in indicator:
                                    indicator[functie_niveau] = {}
                                    indicator[functie_niveau]['Totaal'] = 0
                                    indicator[functie_niveau][werkovereenkomst_type] = uren
                                    
                                elif werkovereenkomst_type not in indicator[functie_niveau]:
                                    indicator[functie_niveau][werkovereenkomst_type] = uren
                                else:
                                    indicator[functie_niveau][werkovereenkomst_type] += uren
                                indicator[functie_niveau]['Totaal'] += uren
    indicator_string = f"\nIGJ 1.2.3: peildatum {peildatum}, vestiging {vestiging}:"                                
    for niveau in indicator:
        indicator_string += f"\n    {str(niveau).split('#')[1]}: "
        for type in indicator[niveau]:
            if type == 'Totaal':
                type_label = 'Totaal'
            else:
                type_label = str(type).split('#')[1]
            indicator_string += f"{type_label}: {indicator[niveau][type]}, "
        # indicator_string += '\n'
    indicator_string += f"\n    Totaal: {gewerkte_uren}"
    return (indicator_string)