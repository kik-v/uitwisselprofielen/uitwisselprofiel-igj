def indicator_igj_1_2_1(peildatum, vestiging, mensen):
    indicator = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            type_overeenkomst = werkovereenkomst.get_rdftype().split('/')[-1]
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                contract_vestiging = werkovereenkomstafspraak.get_werklocatie()
                if contract_vestiging == vestiging or not vestiging:
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(peildatum, contract_periode):
                        functie = werkovereenkomstafspraak.get_functie()
                        niveau = FunctieNiveaus.get(functie)
                        if niveau:
                            if niveau not in indicator:
                                indicator[niveau] = {}
                                indicator[niveau]["Totaal"] = 0
                            if type_overeenkomst not in indicator[niveau]:
                                indicator[niveau][type_overeenkomst] = 0
                            indicator[niveau][type_overeenkomst] += 1    
                            indicator[niveau]["Totaal"] += 1

    output_string = f"\nIGJ 1.2.1 indicator peildatum {peildatum}, vestiging {vestiging}:"
    sorted_dict = {key: indicator[key] for key in sorted(indicator.keys())}
    for niveau in sorted_dict:
        output_string += '\n    ' + str(niveau.split('/')[-1]) + ':'
        for type_overeenkomst in indicator[niveau]:
            output_string += ' ' + type_overeenkomst.split('#')[-1] + ': ' + str(indicator[niveau][type_overeenkomst])
    return (output_string)
