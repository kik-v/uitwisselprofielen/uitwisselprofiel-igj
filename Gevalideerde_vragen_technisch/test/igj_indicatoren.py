def indicator_igj_1_2_3(peildatum, vestiging, mensen):
    gewerkte_uren = 0
    indicator = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            werkovereenkomst_type = werkovereenkomst.get_rdftype()
            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                functie = werkovereenkomstafspraak.get_functie()
                if functie in ZorgFuncties:
                    functie_niveau = FunctieNiveaus[functie]
                    afspraak_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(peildatum, afspraak_periode):
                        for gewerkte_periode in gewerkte_perioden:
                            uren = gewerkte_periode.get_uren()
                            dag = gewerkte_periode.get_date()
                            if dag == peildatum and (gewerkte_periode.get_locatie() == vestiging): #(date_in_period(dag,(peildatum - timedelta(days=99), peildatum + timedelta(days=99)))) and (gewerkte_periode.get_locatie() == dummy.Vestiging_De_Beuk):
                                gewerkte_uren += uren
                                if functie_niveau not in indicator:
                                    indicator[functie_niveau] = {}
                                    indicator[functie_niveau]['Totaal'] = 0
                                    indicator[functie_niveau][werkovereenkomst_type] = uren
                                    
                                elif werkovereenkomst_type not in indicator[functie_niveau]:
                                    indicator[functie_niveau][werkovereenkomst_type] = uren
                                else:
                                    indicator[functie_niveau][werkovereenkomst_type] += uren
                                indicator[functie_niveau]['Totaal'] += uren
    indicator_string = f"IGJ 1.2.3: \n"                                
    for niveau in indicator:
        indicator_string += f"{str(niveau).split('#')[1]}: "
        for type in indicator[niveau]:
            if type == 'Totaal':
                type_label = 'Totaal'
            else:
                type_label = str(type).split('#')[1]
            indicator_string += f"{type_label}: {indicator[niveau][type]}, "
        indicator_string += '\n'
    indicator_string += f"Totaal: {gewerkte_uren}"
    return (indicator_string)

def indicator_igj_1_4_1(peildatum, vestiging, mensen):
    dag_inzet = set()
    avond_inzet = set()
    nacht_inzet = set()
    indicator = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            werkovereenkomst_type = werkovereenkomst.get_rdftype()
            if werkovereenkomst_type in ArbeidsOvereenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    functie = werkovereenkomstafspraak.get_functie()
                    if functie in ZorgFuncties:
                        functie_niveau = FunctieNiveaus[functie]
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        if date_in_period(peildatum, afspraak_periode):
                            for gewerkte_periode in gewerkte_perioden:
                                dag = gewerkte_periode.get_date()
                                if (dag == peildatum) and (gewerkte_periode.get_locatie() == vestiging):                                 
                                    start_uur = gewerkte_periode.get_starttime().hour
                                    if start_uur >= 7 and start_uur < 16:
                                        if mens not in dag_inzet:
                                            if functie_niveau not in indicator:
                                                indicator[functie_niveau] = {"dag":0, "avond":0, "nacht":0}
                                            indicator[functie_niveau]["dag"] += 1
                                        dag_inzet.add(mens)
                                    elif start_uur >= 16 and start_uur < 23:
                                        if mens not in avond_inzet:
                                            if functie_niveau not in indicator:
                                                indicator[functie_niveau] = {"dag":0, "avond":0, "nacht":0}
                                            indicator[functie_niveau]["avond"] += 1
                                        avond_inzet.add(mens)
                                    else:
                                        if mens not in nacht_inzet:
                                            if functie_niveau not in indicator:
                                                indicator[functie_niveau] = {"dag":0, "avond":0, "nacht":0}
                                            indicator[functie_niveau]["nacht"] += 1
                                        nacht_inzet.add(mens)

    indicator_string = f"IGJ 1.4.1,\n    peildatum: {peildatum}\n    vestiging: {vestiging} \n"                                
    for nivo in dict(sorted(indicator.items())):
        indicator_string += (f"\n{nivo}: ")
        for dienst, aantal in indicator[nivo].items():
            indicator_string += (f"{dienst}: {aantal}  ")
    return (indicator_string)