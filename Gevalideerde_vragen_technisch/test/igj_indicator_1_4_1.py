def indicator_igj_1_4_1(peildatum, vestiging, mensen):
    indicator = {}
    for functie_niveau in FunctieNiveaus.values():
        indicator[functie_niveau] = {
            "dag_aantal_pil": set(),
            "avond_aantal_pil": set(),
            "nacht_aantal_pil": set(),
            "dag_aantal_pnil": set(),
            "avond_aantal_pnil": set(),
            "nacht_aantal_pnil": set()
        }
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            werkovereenkomst_type = werkovereenkomst.get_rdftype()
            geldige_overeenkomst = ArbeidsOvereenkomsten + PnilOvereenkomsten
            if werkovereenkomst_type in geldige_overeenkomst:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    functie = werkovereenkomstafspraak.get_functie()
                    if functie in ZorgFuncties:
                        functie_niveau = FunctieNiveaus[functie]
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        if date_in_period(peildatum, afspraak_periode):
                            for gewerkte_periode in gewerkte_perioden:
                                dag = gewerkte_periode.get_date()
                                if (dag == peildatum) and gewerkte_periode.get_locatie() == vestiging:  
                                    pil_pnil = "pil" if werkovereenkomst_type in ArbeidsOvereenkomsten else "pnil"
                                    start_uur = gewerkte_periode.get_starttime().hour
                                    if start_uur >= 7 and start_uur < 16:
                                        inzet = "dag_aantal"
                                    elif start_uur >= 16 and start_uur < 23:
                                        inzet = "avond_aantal"
                                    else:
                                        inzet = "nacht_aantal"    
                                    pointer = inzet + '_' + pil_pnil
                                    indicator[functie_niveau][pointer].add(mens)
    indicator_string = f"\nIGJ 1.4.1,    peildatum: {peildatum}    vestiging: {vestiging}"                                
    for nivo in dict(sorted(indicator.items())):
        indicator_string += (f"\n    {nivo.split('#')[1]}: ")
        for dienst, aantal in indicator[nivo].items():
            indicator_string += (f"{dienst}: {len(aantal)}  ")
    return (indicator_string)
